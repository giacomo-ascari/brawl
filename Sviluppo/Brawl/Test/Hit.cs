﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Test
{
    class Hit
    {
        Point point;
        HitType hitType;

        public Hit(Point point, HitType hitType)
        {
            this.point = point;
            this.hitType = hitType;
        }


        public Point Point { get => point; set => point = value; }
        public HitType HitType { get => hitType; set => hitType = value; }

        public override string ToString()
        {
            return point.X + ";" + point.Y + ";" + hitType.ToString();
        }

        public static Hit Parse(string str)
        {
            string[] v = str.Split(';');
            HitType hitType;
            Enum.TryParse<HitType>(v[2], out hitType);
            Hit hit = new Hit(new Point(Int32.Parse(v[0]), Int32.Parse(v[1])), hitType);
            return hit;
        }
    }
}
