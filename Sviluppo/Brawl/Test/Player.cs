﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace Test
{
    class Player
    {
        Class playerClass;
        Mode mode;
        string dirPath;

        int health;
        double defense;
        double summon;


        int xSpeed;
        int ySpeed;
        int maxXSpeed;
        int maxXSpeedReduced;
        bool fighting;
        bool jumping;
        bool walking;
        bool defending;
        bool summoning;

        int xRender;
        int yRender;
        int x;
        int y;
        int hbWidth;
        int hbHeight;
        int direction;
        string name = "";

        Image banner;
        List<Image> torsoWalkingAnim;
        List<Image> torsoStillAnim;
        List<Image> torsoJumpingAnim;
        List<Image> armsFightingAnim;
        List<Image> armsWalkingAnim;
        List<Image> armsStillAnim;
        List<Image> armsJumpingAnim;
        List<Hit> performedHits;
        List<Image> defenceAnim;
        List<Image> summonAnim;

        int walkingFrame;
        int fightingFrame;
        int defenceFrame;
        int summonFrame;

        public int Health { get => health; set => health = value; }
        public double Defense { get => defense; set => defense = value; }
        public int XSpeed { get => xSpeed; set => xSpeed = value; }
        public int YSpeed { get => ySpeed; set => ySpeed = value; }
        public int MaxXSpeed { get => maxXSpeed; set => maxXSpeed = value; }
        public int MaxXSpeedReduced { get => maxXSpeedReduced; set => maxXSpeedReduced = value; }
        public bool Fighting { get => fighting; set => fighting = value; }
        public bool Jumping { get => jumping; set => jumping = value; }
        public bool Walking { get => walking; set => walking = value; }
        public bool Defending { get => defending; set => defending = value; }
        public int XRender { get => xRender; set => xRender = value; }
        public int YRender { get => yRender; set => yRender = value; }
        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public int HbWidth { get => hbWidth; set => hbWidth = value; }
        public int HbHeight { get => hbHeight; set => hbHeight = value; }
        public int Direction { get => direction; set => direction = value; }
        public string Name { get => name; set => name = value; }
        public Image Banner { get => banner; set => banner = value; }
        public List<Image> TorsoWalkingAnim { get => torsoWalkingAnim; set => torsoWalkingAnim = value; }
        public List<Image> TorsoStillAnim { get => torsoStillAnim; set => torsoStillAnim = value; }
        public List<Image> TorsoJumpingAnim { get => torsoJumpingAnim; set => torsoJumpingAnim = value; }
        public List<Image> ArmsFightingAnim { get => armsFightingAnim; set => armsFightingAnim = value; }
        public List<Image> ArmsWalkingAnim { get => armsWalkingAnim; set => armsWalkingAnim = value; }
        public List<Image> ArmsStillAnim { get => armsStillAnim; set => armsStillAnim = value; }
        public List<Image> ArmsJumpingAnim { get => armsJumpingAnim; set => armsJumpingAnim = value; }
        public List<Hit> PerformedHits { get => performedHits; set => performedHits = value; }
        public List<Image> DefenceAnim { get => defenceAnim; set => defenceAnim = value; }
        public int WalkingFrame { get => walkingFrame; set => walkingFrame = value; }
        public int FightingFrame { get => fightingFrame; set => fightingFrame = value; }
        public int DefenceFrame { get => defenceFrame; set => defenceFrame = value; }
        public Class PlayerClass
        {
            get => playerClass; set
            {
                playerClass = value;
                string path = dirPath + "\\" + playerClass.ToString() + "\\" + (int)mode;
                banner = Image.FromFile(path + "\\banner.png");
                torsoWalkingAnim = new List<Image>
                {
                    Image.FromFile(path + "\\torso1_right.png"),
                    Image.FromFile(path + "\\torso2_right.png"),
                    Image.FromFile(path + "\\torso1_right.png"),
                    Image.FromFile(path + "\\torso3_right.png"),
                    Image.FromFile(path + "\\torso1_left.png"),
                    Image.FromFile(path + "\\torso2_left.png"),
                    Image.FromFile(path + "\\torso1_left.png"),
                    Image.FromFile(path + "\\torso3_left.png")
                };
                torsoStillAnim = new List<Image>
                {
                    Image.FromFile(path + "\\torso1_right.png"),
                    Image.FromFile(path + "\\torso1_left.png")
                };
                torsoJumpingAnim = new List<Image>
                {
                    Image.FromFile(path + "\\torso3_right.png"),
                    Image.FromFile(path + "\\torso3_left.png")
                };
                armsFightingAnim = new List<Image>
                {
                    Image.FromFile(path + "\\fighting1_right.png"),
                    Image.FromFile(path + "\\fighting2_right.png"),
                    Image.FromFile(path + "\\fighting3_right.png"),
                    Image.FromFile(path + "\\fighting1_left.png"),
                    Image.FromFile(path + "\\fighting2_left.png"),
                    Image.FromFile(path + "\\fighting3_left.png")
                };
                armsWalkingAnim = new List<Image>
                {
                    Image.FromFile(path + "\\walking1_right.png"),
                    Image.FromFile(path + "\\walking2_right.png"),
                    Image.FromFile(path + "\\walking1_right.png"),
                    Image.FromFile(path + "\\walking3_right.png"),
                    Image.FromFile(path + "\\walking1_left.png"),
                    Image.FromFile(path + "\\walking2_left.png"),
                    Image.FromFile(path + "\\walking1_left.png"),
                    Image.FromFile(path + "\\walking3_left.png")
                };
                armsStillAnim = new List<Image>
                {
                    Image.FromFile(path + "\\walking1_right.png"),
                    Image.FromFile(path + "\\walking1_left.png")
                };
                armsJumpingAnim = new List<Image>
                {
                    Image.FromFile(path + "\\walking3_right.png"),
                    Image.FromFile(path + "\\walking3_left.png")
                };
                defenceAnim = new List<Image>
                {
                    Image.FromFile(path + "\\shield1_right.png"),
                    Image.FromFile(path + "\\shield2_right.png"),
                    Image.FromFile(path + "\\shield3_right.png"),
                    Image.FromFile(path + "\\shield4_right.png"),
                    Image.FromFile(path + "\\shield1_left.png"),
                    Image.FromFile(path + "\\shield2_left.png"),
                    Image.FromFile(path + "\\shield3_left.png"),
                    Image.FromFile(path + "\\shield4_left.png")
                };
                summonAnim = new List<Image>
                {
                    Image.FromFile(path + "\\ability1_right.png"),
                    Image.FromFile(path + "\\ability2_right.png"),
                    Image.FromFile(path + "\\ability3_right.png"),
                    Image.FromFile(path + "\\ability4_right.png"),
                    Image.FromFile(path + "\\ability1_left.png"),
                    Image.FromFile(path + "\\ability2_left.png"),
                    Image.FromFile(path + "\\ability3_left.png"),
                    Image.FromFile(path + "\\ability4_left.png")
                };
            }
        }
        public Mode Mode { get => mode; set => mode = value; }
        public string DirPath { get => dirPath; set => dirPath = value; }
        public int SummonFrame { get => summonFrame; set => summonFrame = value; }
        public List<Image> SummonAnim { get => summonAnim; set => summonAnim = value; }
        public bool Summoning { get => summoning; set => summoning = value; }
        public double Summon { get => summon; set => summon = value; }

        public void GetSummon(int opponentDirection)
        {
            if (opponentDirection != direction && !summoning)
                summon += 7;
        }
        public bool TakeHit(HitType hit)
        {
            int hitIntensity = 10;
            if (hit == HitType.High) hitIntensity *= 2;
            else if (hit == HitType.Low) hitIntensity /= 2;
            if (defending)
            {
                defenceFrame += 1;
                if (defenceFrame == defenceAnim.Count / 2)
                {
                    defending = false;
                }
                return false;
            }
            else if (summoning)
            {
                if (playerClass == Class.Crusader)
                {
                    health -= hitIntensity / 2;
                    ySpeed = 6;
                }
                else if (playerClass == Class.Viking)
                {

                }
                else if (playerClass == Class.Saracen)
                {

                }
                else if (playerClass == Class.Samurai)
                {

                }
                return true;
            }
            else
            {
                health -= hitIntensity;
                ySpeed = 6;
                return true;
            }
        }
        public Image Render()
        {
            Bitmap bitmap;
            Graphics g;
            if (summoning)
            {
                if (playerClass == Class.Viking)
                {
                    bitmap = new Bitmap(RenderTorso());
                    g = Graphics.FromImage(bitmap);
                    g.DrawImage(RenderArms(), 0, 0);
                    g.DrawImage(RenderSummoning(), 0, 0);
                    g.Dispose();
                }
                else if (playerClass == Class.Samurai)
                {
                    bitmap = new Bitmap(RenderSummoning());
                }
                else
                {
                    bitmap = new Bitmap(RenderSummoning());
                    g = Graphics.FromImage(bitmap);
                    g.DrawImage(RenderTorso(), 0, 0);
                    g.DrawImage(RenderArms(), 0, 0);
                    g.Dispose();
                }
            }
            else
            {
                bitmap = new Bitmap(RenderTorso());
                g = Graphics.FromImage(bitmap);
                g.DrawImage(RenderArms(), 0, 0);
                g.Dispose();
            }
            /*using (Graphics g = Graphics.FromImage(frame))
            {
                g.DrawImageUnscaled(RenderArms(), 0, 0);
            }*/
            return bitmap;
        }
        public Image RenderSummoning()
        {
            if (playerClass == Class.Crusader)
            {
                summonFrame++;
                int shift = summonAnim.Count / 2 * direction;
                int i = shift + (int)summonFrame % 16 / 4;
                xRender = x - summonAnim[i].Width / 2;
                yRender = 528 - 96 - y - summonAnim[i].Height;
                return summonAnim[i];
            }
            else if (playerClass == Class.Viking)
            {
                summonFrame++;
                int shift = summonAnim.Count / 2 * direction;
                int i = shift + (int)summonFrame % 16 / 4;
                xRender = x - summonAnim[i].Width / 2;
                yRender = 528 - 96 - y - summonAnim[i].Height;
                return summonAnim[i];
            }
            else if (playerClass == Class.Saracen)
            {
                summonFrame++;
                int shift = summonAnim.Count / 2 * direction;
                int i = shift + (int)summonFrame % 16 / 4;
                xRender = x - summonAnim[i].Width / 2;
                yRender = 528 - 96 - y - summonAnim[i].Height;
                if (summonFrame % 10 == 0)
                {
                    performedHits.Add(new Hit(new Point(this.x + 50, y + hbHeight / 2), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x + 35, y + hbHeight / 4 * 3), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x + 35, y + hbHeight / 4 * 1), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x + 0, y + hbHeight / 2), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x + 0, y + 0), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x - 50, y + hbHeight / 2), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x - 35, y + hbHeight / 4 * 3), HitType.Low));
                    performedHits.Add(new Hit(new Point(this.x - 35, y + hbHeight / 4 * 1), HitType.Low));
                }
                return summonAnim[i];
            }
            else //if (playerClass == Class.Samurai)
            {
                summonFrame++;
                int shift = summonAnim.Count / 2 * direction;
                int i = shift;
                xRender = x - summonAnim[i].Width / 2;
                yRender = 528 - 96 - y - summonAnim[i].Height;

                if (direction == 0) performedHits.Add(new Hit(new Point(this.x + 50, y + hbHeight / 2), HitType.Normal));
                else performedHits.Add(new Hit(new Point(this.x - 50, y + hbHeight / 2), HitType.Normal));
                
                return summonAnim[i];
            }
        }

        public Image RenderTorso()
        {
            if (jumping)
            {
                int shift = torsoJumpingAnim.Count / 2 * direction;
                int i = shift;
                xRender = x - torsoJumpingAnim[i].Width / 2;
                yRender = 528 - 96 - y - torsoJumpingAnim[i].Height;
                return torsoJumpingAnim[i];
            }
            else if (walking)
            {
                walkingFrame++;
                int shift = torsoWalkingAnim.Count / 2 * direction;
                int i = shift + (int)walkingFrame % 24 / 6;
                xRender = x - torsoWalkingAnim[i].Width / 2;
                yRender = 528 - 96 - y - torsoWalkingAnim[i].Height;
                return torsoWalkingAnim[i];
            }
            else
            {
                int shift = torsoStillAnim.Count / 2 * direction;
                int i = shift;
                xRender = x - torsoStillAnim[i].Width / 2;
                yRender = 528 - 96 - y - torsoStillAnim[i].Height;
                return torsoStillAnim[i];
            }
        }
        public Image RenderArms()
        {
            if (defending)
            {
                int shift = defenceAnim.Count / 2 * direction;
                int i = shift + defenceFrame;
                xRender = x - defenceAnim[i].Width / 2;
                yRender = 528 - 96 - y - defenceAnim[i].Height;
                return defenceAnim[i];
            }
            if (fighting)
            {
                int shift = armsFightingAnim.Count / 2 * direction;
                int i = shift + (int)fightingFrame % 9 / 3;
                xRender = x - armsFightingAnim[i].Width / 2;
                yRender = 528 - 96 - y - armsFightingAnim[i].Height;
                if (fightingFrame == 8)
                {
                    fightingFrame = 0;
                    fighting = false;
                    int x = this.x;
                    if (direction == 0)
                    {
                        x += 50;
                    }
                    else
                    {
                        x -= 50;
                    }
                    if (playerClass == Class.Viking && summoning) performedHits.Add(new Hit(new Point(x, y + hbHeight / 2), HitType.High));
                    else performedHits.Add(new Hit(new Point(x, y + hbHeight / 2), HitType.Normal));
                }
                else
                {
                    fightingFrame++;
                }

                return armsFightingAnim[i];
            }
            else
            {
                fightingFrame = 0;
                if (jumping)
                {
                    int shift = armsJumpingAnim.Count / 2 * direction;
                    int i = shift;
                    xRender = x - armsJumpingAnim[i].Width / 2;
                    yRender = 528 - 96 - y - armsJumpingAnim[i].Height;
                    return armsJumpingAnim[i];
                }
                else if (walking)
                {
                    int shift = armsWalkingAnim.Count / 2 * direction;
                    int i = shift + (int)walkingFrame % 24 / 6;
                    xRender = x - armsWalkingAnim[i].Width / 2;
                    yRender = 528 - 96 - y - armsWalkingAnim[i].Height;
                    return armsWalkingAnim[i];
                }
                else
                {
                    int shift = armsStillAnim.Count / 2 * direction;
                    int i = shift;
                    xRender = x - armsStillAnim[i].Width / 2;
                    yRender = 528 - 96 - y - armsStillAnim[i].Height;
                    return armsStillAnim[i];
                }
            }
        }

        public static byte GetVirtualKeyCode(Keys key)
        {
            int value = (int)key;
            return (byte)(value & 0xFF);
        }
        public void Update(byte[] array)
        {
            int maxXSpeed;
            //fighting = false;
            walking = false;
            jumping = false;
            

            if (defending)
            {
                maxXSpeed = maxXSpeedReduced;
                defense -= 0.3;
                if (defense <= 0) defending = false;
            }
            else
            {
                maxXSpeed = this.maxXSpeed;
                defense += 0.15;
                if (defense > 80) defense = 80;
            }

            if (summoning)
            {
                if (playerClass == Class.Viking) summon -= 0.5;
                if (playerClass == Class.Samurai) summon -= 1.5;
                else summon -= 0.3;
                if (summon <= 0) summoning = false;
            }
            else
            {
                summon += 0.1;
                //summon += 0.5;
                if (summon > 80) summon = 80;
            }

            if ((array[GetVirtualKeyCode(Keys.C)] & 0x80) != 0 && summon == 80 && !defending)
            {
                summonFrame = 0;
                summoning = true;
            }

            if ((array[GetVirtualKeyCode(Keys.X)] & 0x80) != 0 && defense == 80 && !summoning)
            {
                defenceFrame = 0;
                defending = true;
            }
            
            if ((array[GetVirtualKeyCode(Keys.Z)] & 0x80) != 0) fighting = true;

            if (playerClass == Class.Samurai && summoning)
            {
                if (direction == 0) xSpeed = 25;
                else xSpeed = -25;
            }
            else
            {
                if ((array[GetVirtualKeyCode(Keys.Up)] & 0x80) != 0 && y == 0)
                {
                    if (defending) ySpeed = +16;
                    else ySpeed = +20;
                }
                if (y > 0) jumping = true;
                ySpeed -= 2;

                y += ySpeed;
                y = Math.Max(y, 0);

                if ((array[GetVirtualKeyCode(Keys.Left)] & 0x80) != 0)
                {
                    walking = true;
                    xSpeed -= 2;
                    if (xSpeed < -maxXSpeed)
                        xSpeed = -maxXSpeed;
                    direction = 1;
                }
                else if ((array[GetVirtualKeyCode(Keys.Right)] & 0x80) != 0)
                {
                    walking = true;
                    xSpeed += 2;
                    if (xSpeed > maxXSpeed)
                        xSpeed = maxXSpeed;
                    direction = 0;
                }
                else
                {
                    if (y == 0) xSpeed /= 2;
                }
            }
            
            x += xSpeed;
            if (x < hbWidth / 2)
            {
                x = hbWidth / 2;
                if (playerClass == Class.Samurai && summoning)
                {
                    summoning = false;
                    summon = 0;
                }
            }
            if (x > 960 - hbWidth / 2)
            {
                x = 960 - hbWidth / 2;
                if (playerClass == Class.Samurai && summoning)
                {
                    summoning = false;
                    summon = 0;
                }
            }

        }

        public Player()
        {
            performedHits = new List<Hit>();
            maxXSpeed = 7;
            maxXSpeedReduced = 3;
            health = 100;
            defense = 80;
        }
    }
}
