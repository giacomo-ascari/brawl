﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Test
{
    class Text
    {
        Bitmap image;
        string str;


        public Text(Bitmap image, string text) 
        {
            this.image = image;
            this.str = text;
        }

        public Bitmap Image { get => image; set => image = value; }
        public string Str { get => str; set => str = value; }
    }

    class Writer
    {
        public List<Text> list = new List<Text>();

        static List<Image> chars = new List<Image>();

        public List<Image> Chars { get => chars; set => chars = value; }

        public Bitmap GetImageFromString(string text)
        {
            text = text.ToUpper();

            int pos = list.FindIndex(obj => obj.Str == text);
            if (pos == -1)
            {
                int height = chars[0].Height;
                int width = 0;
                foreach (char c in text)
                {
                    width += chars[(int)c].Width;
                }
                Bitmap image = new Bitmap(Math.Max(1, width), height);
                Graphics graphics = Graphics.FromImage(image);
                int x = 0;
                foreach (char c in text)
                {
                    graphics.DrawImage(chars[(int)c], x, 0);
                    x += chars[(int)c].Width;
                }
                list.Add(new Text(image, text));
                return image;
            }
            else
            {
                return list[pos].Image;
            }
            
        }

        public Writer(string dir)
        {
            chars = new List<Image>();
            for (int i = 0; i < 128; i++)
            {
                try
                {
                    chars.Add(Image.FromFile(dir + "\\" + i + ".png"));
                }
                catch
                {
                    chars.Add(Image.FromFile(dir + "\\88.png"));
                }
            }
        }

    }
}
