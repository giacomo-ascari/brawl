﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Net.NetworkInformation;

namespace Test
{

    public partial class GameWindow : Form
    {
        private Mode mode;
        private IPAddress ip;
        private int port;
        private Class playerClass;

        private State state;

        Socket socket;

        Thread t;

        Image gameBackground;
        Image guiHealthPlus;
        Image guiHealthMinus;
        Image guiDefensePlus;
        Image guiDefenseMinus;
        Image guiSummonPlus;
        Image guiSummonMinus;
        Image waitingBackground;
        List<Image> waitingForeground;
        List<Image> preparationForeground;
        Image wonForeground;
        Image lostForeground;
        Image disconnectedForeground;


        int waitingFrame;
        int preparationFrame;
        Image scene;

        Player player1;
        Player player2;
        Stopwatch timeout;
        System.Windows.Forms.Timer GameTimer;

        Writer writer;

        public void GameUpdate()
        {
            try
            {
                socket.Send(Encoding.ASCII.GetBytes("CL=" + player1.PlayerClass.ToString() + "|"));
                socket.Send(Encoding.ASCII.GetBytes("N=" + player1.Name + "|"));
                while (state == State.Game || state == State.Preparation)
                {
                    timeout.Restart();
                    string data = null;
                    byte[] bytes = new Byte[1024];
                    while (state == State.Game || state == State.Preparation)
                    {
                        int bytesRec = 0;
                        bytesRec = socket.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf('|') > -1)
                        {
                            break;
                        }
                    }
                    try
                    {
                        foreach (string d in data.Split('|'))
                        {
                            string[] s = d.Split('=');
                            switch (s[0])
                            {
                                case "SP":
                                    player1.GetSummon(player2.Direction);
                                    break;
                                case "S":
                                    player2.Summon = Double.Parse(s[1]);
                                    break;
                                case "SA":
                                    player2.Summoning = Boolean.Parse(s[1]);
                                    break;
                                case "CL":
                                    Class playerClass;
                                    Enum.TryParse<Class>(s[1], out playerClass);
                                    player2.PlayerClass = playerClass;
                                    break;
                                case "N":
                                    player2.Name = s[1];
                                    break;
                                case "X":
                                    player2.X = Int32.Parse(s[1]);
                                    break;
                                case "Y":
                                    player2.Y = Int32.Parse(s[1]);
                                    break;
                                case "D":
                                    player2.Direction = Int32.Parse(s[1]);
                                    break;
                                case "J":
                                    player2.Jumping = Boolean.Parse(s[1]);
                                    break;
                                case "W":
                                    player2.Walking = Boolean.Parse(s[1]);
                                    break;
                                case "F":
                                    player2.Fighting = Boolean.Parse(s[1]);
                                    break;
                                case "DE":
                                    player2.Defense = Double.Parse(s[1]);
                                    break;
                                case "DA":
                                    player2.Defending = Boolean.Parse(s[1]);
                                    break;
                                case "DF":
                                    player2.DefenceFrame = Int32.Parse(s[1]);
                                    break;
                                case "H":
                                    player2.Health = Int32.Parse(s[1]);
                                    break;
                                case "Hit":
                                    Hit hit = Hit.Parse(s[1]);
                                    if (hit.Point.X <= player1.X + player1.HbWidth / 2 && hit.Point.X >= player1.X - player1.HbWidth / 2)
                                    {
                                        if (hit.Point.Y <= player1.Y + player1.HbHeight / 2 && hit.Point.Y >= player1.Y - player1.HbHeight / 2)
                                        {
                                            if (player1.TakeHit(hit.HitType))
                                            {
                                                socket.Send(Encoding.ASCII.GetBytes("SP|"));
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    catch { }
                }
            }
            catch
            {
                state = State.Disconnected;
                //MessageBox.Show("Died in game update");
            }
            try { socket.Disconnect(true); } catch { }
            try { socket.Shutdown(SocketShutdown.Both); } catch { }
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetKeyboardState(byte[] lpKeyState);

        public void GameTick(object sender, EventArgs e)
        {
            Graphics graphics = Graphics.FromImage(scene);
            if (state == State.Game)
            {
                try
                {
                    byte[] array = new byte[256];
                    GetKeyboardState(array);
                    player1.Update(array);

                    socket.Send(Encoding.ASCII.GetBytes("X=" + player1.X + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("Y=" + player1.Y + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("D=" + player1.Direction + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("J=" + player1.Jumping + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("W=" + player1.Walking + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("F=" + player1.Fighting + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("DE=" + player1.Defense + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("DA=" + player1.Defending + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("DF=" + player1.DefenceFrame + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("S=" + player1.Summon + "|"));
                    socket.Send(Encoding.ASCII.GetBytes("SA=" + player1.Summoning + "|"));

                    graphics.DrawImage(gameBackground, new PointF(0, 0));
                    graphics.DrawImage(player2.Render(), player2.XRender, player2.YRender);
                    graphics.DrawImage(player1.Render(), player1.XRender, player1.YRender);

                    foreach (Hit hit in player1.PerformedHits)
                    {
                        socket.Send(Encoding.ASCII.GetBytes("Hit=" + hit.ToString() + "|"));
                    }
                    player1.PerformedHits.Clear();

                    if (mode == Mode.Host)
                    {
                        graphics.DrawImage(player1.Banner, 0, 0);
                        graphics.DrawImage(player2.Banner, 960 - player2.Banner.Width, 0);
                        graphics.DrawImage(writer.GetImageFromString(player1.Name.ToString()), player1.Banner.Width, 8);
                        Image joinerName = writer.GetImageFromString(player2.Name.ToString());
                        graphics.DrawImage(joinerName, 960 - player2.Banner.Width - joinerName.Width, 8);
                        for (int i = 0; i < 100; i++)
                        {
                            if (i < player1.Health)
                                graphics.DrawImage(guiHealthPlus, player1.Banner.Width + i * guiHealthPlus.Width, 32);
                            else
                                graphics.DrawImage(guiHealthMinus, player1.Banner.Width + i * guiHealthMinus.Width, 32);
                            if (i < player2.Health)
                                graphics.DrawImage(guiHealthPlus, 960 - player2.Banner.Width - i * guiHealthPlus.Width, 32);
                            else
                                graphics.DrawImage(guiHealthMinus, 960 - player2.Banner.Width - i * guiHealthMinus.Width, 32);
                        }
                        for (int i = 0; i < 80; i++)
                        {
                            if (i < (int)player1.Defense)
                                graphics.DrawImage(guiDefensePlus, player1.Banner.Width + i * guiDefensePlus.Width, 56);
                            else
                                graphics.DrawImage(guiDefenseMinus, player1.Banner.Width + i * guiDefenseMinus.Width, 56);
                            if (i < (int)player2.Defense)
                                graphics.DrawImage(guiDefensePlus, 960 - player1.Banner.Width - i * guiDefensePlus.Width, 56);
                            else
                                graphics.DrawImage(guiDefenseMinus, 960 - player1.Banner.Width - i * guiDefenseMinus.Width, 56);
                        }
                        for (int i = 0; i < 80; i++)
                        {
                            if (i < (int)player1.Summon)
                                graphics.DrawImage(guiSummonPlus, player1.Banner.Width + i * guiSummonPlus.Width, 80);
                            else
                                graphics.DrawImage(guiSummonMinus, player1.Banner.Width + i * guiSummonMinus.Width, 80);
                            if (i < (int)player2.Summon)
                                graphics.DrawImage(guiSummonPlus, 960 - player1.Banner.Width - i * guiSummonPlus.Width, 80);
                            else
                                graphics.DrawImage(guiSummonMinus, 960 - player1.Banner.Width - i * guiSummonMinus.Width, 80);
                        }
                    }
                    else if (mode == Mode.Joiner)
                    {
                        graphics.DrawImage(player2.Banner, 0, 0);
                        graphics.DrawImage(player1.Banner, 960 - player1.Banner.Width, 0);
                        graphics.DrawImage(writer.GetImageFromString(player2.Name.ToString()), player2.Banner.Width, 8);
                        Image joinerName = writer.GetImageFromString(player1.Name.ToString());
                        graphics.DrawImage(joinerName, 960 - player2.Banner.Width - joinerName.Width, 8);
                        for (int i = 0; i < 100; i++)
                        {
                            if (i < player2.Health)
                                graphics.DrawImage(guiHealthPlus, player2.Banner.Width + i * guiHealthPlus.Width, 32);
                            else
                                graphics.DrawImage(guiHealthMinus, player2.Banner.Width + i * guiHealthMinus.Width, 32);
                            if (i < player1.Health)
                                graphics.DrawImage(guiHealthPlus, 960 - player1.Banner.Width - i * guiHealthPlus.Width, 32);
                            else
                                graphics.DrawImage(guiHealthMinus, 960 - player1.Banner.Width - i * guiHealthMinus.Width, 32);
                        }
                        for (int i = 0; i < 80; i++)
                        {
                            if (i < (int)player2.Defense)
                                graphics.DrawImage(guiDefensePlus, player2.Banner.Width + i * guiDefensePlus.Width, 56);
                            else
                                graphics.DrawImage(guiDefenseMinus, player2.Banner.Width + i * guiDefenseMinus.Width, 56);
                            if (i < (int)player1.Defense)
                                graphics.DrawImage(guiDefensePlus, 960 - player1.Banner.Width - i * guiDefensePlus.Width, 56);
                            else
                                graphics.DrawImage(guiDefenseMinus, 960 - player1.Banner.Width - i * guiDefenseMinus.Width, 56);
                        }
                        for (int i = 0; i < 80; i++)
                        {
                            if (i < (int)player2.Summon)
                                graphics.DrawImage(guiSummonPlus, player2.Banner.Width + i * guiSummonPlus.Width, 80);
                            else
                                graphics.DrawImage(guiSummonMinus, player2.Banner.Width + i * guiSummonMinus.Width, 80);
                            if (i < (int)player1.Summon)
                                graphics.DrawImage(guiSummonPlus, 960 - player1.Banner.Width - i * guiSummonPlus.Width, 80);
                            else
                                graphics.DrawImage(guiSummonMinus, 960 - player1.Banner.Width - i * guiSummonMinus.Width, 80);
                        }
                    }

                    socket.Send(Encoding.ASCII.GetBytes("H=" + player1.Health + "|"));

                    if (player1.Health <= 0) state = State.Lost;
                    else if (player2.Health <= 0) state = State.Won;

                    if (timeout.ElapsedMilliseconds > 1500)
                    {
                        throw new Exception();
                    }
                }
                catch /*(Exception ex)*/
                {
                    state = State.Disconnected;
                    //MessageBox.Show("Died in game tick");
                    //MessageBox.Show(ex.ToString());
                }
                
            }
            else if (state == State.Preparation)
            {
                graphics.DrawImage(waitingBackground, 0, 0);
                graphics.DrawImage(preparationForeground[preparationFrame % 150 / 50], 0, 0);
                if (preparationFrame % 150 == 149) state = State.Game;
                else preparationFrame++;
                timeout.Restart();
            }
            else if (state == State.Waiting)
            {
                if (waitingFrame % 2 == 0)
                {
                    graphics.DrawImage(waitingBackground, 0, 0);
                    graphics.DrawImage(waitingForeground[waitingFrame % 45 / 15], 0, 0);
                    graphics.DrawImage(writer.GetImageFromString("Condividi il tuo Indirizzo IP per giocare"), 10, 10);
                    int i = 1;
                    foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        {
                            foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                            {
                                if (ip.Address.AddressFamily == AddressFamily.InterNetwork
                                    && ni.Name.ToLower().IndexOf("virtualbox") == -1
                                    && (ni.Name.ToLower().IndexOf("ethernet") > -1
                                    || ni.Name.ToLower().IndexOf("wi-fi") > -1
                                    || ni.Name.ToLower().IndexOf("wifi") > -1))
                                {
                                    graphics.DrawImage(writer.GetImageFromString(ni.Name + ": " + ip.Address.ToString()), 10, 10 + 30 * i);
                                    i++;
                                }
                            }
                        }
                    }
                }
                waitingFrame++;
            }
            else if (state == State.Disconnected)
            {
                graphics.DrawImage(waitingBackground, 0, 0);
                graphics.DrawImage(disconnectedForeground, 0, 0);
            }
            else if (state == State.Lost)
            {
                graphics.DrawImage(gameBackground, 0, 0);
                graphics.DrawImage(lostForeground, 0, 0);
                graphics.DrawImage(player2.Render(), player2.XRender, player2.YRender);
                graphics.DrawImage(player1.Render(), player1.XRender, player1.YRender);
            }
            else if (state == State.Won)
            {
                graphics.DrawImage(gameBackground, 0, 0);
                graphics.DrawImage(wonForeground, 0, 0);
                graphics.DrawImage(player2.Render(), player2.XRender, player2.YRender);
                graphics.DrawImage(player1.Render(), player1.XRender, player1.YRender);
            }
            gameBox.Image = scene;
        }

        public void HostThread()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, port);
            socket.Bind(localEndPoint);
            socket.Listen(1);
            try
            {
                socket = socket.Accept();
            }
            catch { }
            if (state != State.End)
            {
                player1.PlayerClass = playerClass;
                state = State.Preparation;
                GameUpdate();
            }
            
        }

        public void JoinerThread()
        {
            IPEndPoint remoteEP = new IPEndPoint(ip, port);
            socket.Connect(remoteEP);
            player1.PlayerClass = playerClass;
            state = State.Preparation;
            GameUpdate();
        }

        public GameWindow(Mode mode, IPAddress ip, int port, string name, Class playerClass)
        {
            InitializeComponent();

            this.mode = mode;
            this.ip = ip;
            this.port = port;
            this.playerClass = playerClass;
            //this.Text = this.Text + " " + mode;
            state = State.Waiting;

            timeout = new Stopwatch();

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            Player host = new Player()
            {
                X = 60,
                Y = 0,
                HbWidth = 66,
                HbHeight = 81 + 1,
                Direction = 0,
                DirPath = "assets",
                Mode = Mode.Host
            };

            Player joiner = new Player()
            {
                X = 900,
                Y = 0,
                HbWidth = 66,
                HbHeight = 81 + 1,
                Direction = 1,
                DirPath = "assets",
                Mode = Mode.Joiner
            };

            if (mode == Mode.Host)
            {
                player1 = host;
                player2 = joiner;
            }
            else
            {
                player1 = joiner;
                player2 = host;
            }

            player1.Name = name;

            writer = new Writer("assets\\font");

            guiHealthPlus = Image.FromFile("assets\\gui\\health+.png");
            guiHealthMinus = Image.FromFile("assets\\gui\\health-.png");
            guiDefensePlus = Image.FromFile("assets\\gui\\defense+.png");
            guiDefenseMinus = Image.FromFile("assets\\gui\\defense-.png");
            guiSummonPlus = Image.FromFile("assets\\gui\\summon+.png");
            guiSummonMinus = Image.FromFile("assets\\gui\\summon-.png");
            scene = Image.FromFile("assets\\background\\game_background.png");
            gameBackground = Image.FromFile("assets\\background\\game_background.png");
            waitingBackground = Image.FromFile("assets\\background\\waiting_background.png");
            waitingForeground = new List<Image> {
                Image.FromFile("assets\\background\\waiting1.png"),
                Image.FromFile("assets\\background\\waiting2.png"),
                Image.FromFile("assets\\background\\waiting3.png")
            };
            preparationForeground = new List<Image> {
                Image.FromFile("assets\\background\\preparation1.png"),
                Image.FromFile("assets\\background\\preparation2.png"),
                Image.FromFile("assets\\background\\preparation3.png")
            };
            wonForeground = Image.FromFile("assets\\background\\won.png");
            lostForeground = Image.FromFile("assets\\background\\lost.png");
            disconnectedForeground = Image.FromFile("assets\\background\\disconnected.png");

            GameTimer = new System.Windows.Forms.Timer();
            GameTimer.Interval = 20;
            GameTimer.Tick += new EventHandler(GameTick);
            GameTimer.Start();

            if (mode == Mode.Host) t = new Thread(HostThread);
            else t = new Thread(JoinerThread);
            t.Start();
        }

        private void GameWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (state == State.Waiting)
            {
                
                //t.Interrupt();
                socket.Close();
                state = State.End;
                //t.Abort();
                //socket = null;
                //MessageBox.Show("Waiting close");
            }
            else
            {
                state = State.End;
                t.Join();
                //MessageBox.Show("Normal close");
            }
            
        }
    }
}
