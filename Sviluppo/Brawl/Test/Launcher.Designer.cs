﻿namespace Test
{
    partial class Launcher
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.playerTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.portTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.createRadio = new System.Windows.Forms.RadioButton();
            this.joinRadio = new System.Windows.Forms.RadioButton();
            this.startBtn = new System.Windows.Forms.Button();
            this.addressLbl = new System.Windows.Forms.Label();
            this.addressTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.classCombo = new System.Windows.Forms.ComboBox();
            this.backgroundPBox = new System.Windows.Forms.PictureBox();
            this.infoBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundPBox)).BeginInit();
            this.SuspendLayout();
            // 
            // playerTxt
            // 
            this.playerTxt.Location = new System.Drawing.Point(88, 102);
            this.playerTxt.Name = "playerTxt";
            this.playerTxt.Size = new System.Drawing.Size(296, 20);
            this.playerTxt.TabIndex = 0;
            this.playerTxt.TextChanged += new System.EventHandler(this.goodToStart);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(85, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome del giocatore";
            // 
            // portTxt
            // 
            this.portTxt.Location = new System.Drawing.Point(88, 203);
            this.portTxt.Name = "portTxt";
            this.portTxt.Size = new System.Drawing.Size(296, 20);
            this.portTxt.TabIndex = 2;
            this.portTxt.Text = "42013";
            this.portTxt.TextChanged += new System.EventHandler(this.goodToStart);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Porta (1024-49151)";
            // 
            // createRadio
            // 
            this.createRadio.AutoSize = true;
            this.createRadio.Location = new System.Drawing.Point(88, 229);
            this.createRadio.Name = "createRadio";
            this.createRadio.Size = new System.Drawing.Size(151, 17);
            this.createRadio.TabIndex = 4;
            this.createRadio.TabStop = true;
            this.createRadio.Text = "Crea una partita (localhost)";
            this.createRadio.UseVisualStyleBackColor = true;
            this.createRadio.CheckedChanged += new System.EventHandler(this.goodToStart);
            // 
            // joinRadio
            // 
            this.joinRadio.AutoSize = true;
            this.joinRadio.Location = new System.Drawing.Point(88, 252);
            this.joinRadio.Name = "joinRadio";
            this.joinRadio.Size = new System.Drawing.Size(127, 17);
            this.joinRadio.TabIndex = 5;
            this.joinRadio.TabStop = true;
            this.joinRadio.Text = "Unisciti ad una partita";
            this.joinRadio.UseVisualStyleBackColor = true;
            this.joinRadio.CheckedChanged += new System.EventHandler(this.goodToStart);
            // 
            // startBtn
            // 
            this.startBtn.Enabled = false;
            this.startBtn.Location = new System.Drawing.Point(88, 329);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(296, 23);
            this.startBtn.TabIndex = 6;
            this.startBtn.Text = "INIZIA";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // addressLbl
            // 
            this.addressLbl.AutoSize = true;
            this.addressLbl.Enabled = false;
            this.addressLbl.Location = new System.Drawing.Point(105, 272);
            this.addressLbl.Name = "addressLbl";
            this.addressLbl.Size = new System.Drawing.Size(45, 13);
            this.addressLbl.TabIndex = 7;
            this.addressLbl.Text = "Indirizzo";
            // 
            // addressTxt
            // 
            this.addressTxt.Enabled = false;
            this.addressTxt.Location = new System.Drawing.Point(156, 269);
            this.addressTxt.Name = "addressTxt";
            this.addressTxt.Size = new System.Drawing.Size(162, 20);
            this.addressTxt.TabIndex = 8;
            this.addressTxt.Text = "127.0.0.1";
            this.addressTxt.TextChanged += new System.EventHandler(this.goodToStart);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Classe giocatore";
            // 
            // classCombo
            // 
            this.classCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classCombo.FormattingEnabled = true;
            this.classCombo.Location = new System.Drawing.Point(89, 146);
            this.classCombo.Name = "classCombo";
            this.classCombo.Size = new System.Drawing.Size(263, 21);
            this.classCombo.TabIndex = 10;
            // 
            // backgroundPBox
            // 
            this.backgroundPBox.Location = new System.Drawing.Point(0, 0);
            this.backgroundPBox.Name = "backgroundPBox";
            this.backgroundPBox.Size = new System.Drawing.Size(10, 10);
            this.backgroundPBox.TabIndex = 11;
            this.backgroundPBox.TabStop = false;
            // 
            // infoBtn
            // 
            this.infoBtn.Location = new System.Drawing.Point(358, 145);
            this.infoBtn.Name = "infoBtn";
            this.infoBtn.Size = new System.Drawing.Size(26, 23);
            this.infoBtn.TabIndex = 12;
            this.infoBtn.UseVisualStyleBackColor = true;
            this.infoBtn.Click += new System.EventHandler(this.infoBtn_Click);
            // 
            // Launcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 364);
            this.Controls.Add(this.infoBtn);
            this.Controls.Add(this.backgroundPBox);
            this.Controls.Add(this.classCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addressTxt);
            this.Controls.Add(this.addressLbl);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.joinRadio);
            this.Controls.Add(this.createRadio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.portTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.playerTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Launcher";
            this.Text = "Medieval Brawl Launcher";
            ((System.ComponentModel.ISupportInitialize)(this.backgroundPBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox playerTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox portTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton createRadio;
        private System.Windows.Forms.RadioButton joinRadio;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Label addressLbl;
        private System.Windows.Forms.TextBox addressTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox classCombo;
        private System.Windows.Forms.PictureBox backgroundPBox;
        private System.Windows.Forms.Button infoBtn;
    }
}

