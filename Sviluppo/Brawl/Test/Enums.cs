﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public enum Mode
    {
        Joiner = 2,
        Host = 1
    }

    public enum State
    {
        Game,
        Waiting,
        Preparation,
        End,
        Disconnected,
        Lost,
        Won
    }

    public enum Class
    {
        Crusader,
        Viking,
        Saracen,
        Samurai
    }

    public enum HitType
    {
        Low,
        Normal,
        High
    }
}
