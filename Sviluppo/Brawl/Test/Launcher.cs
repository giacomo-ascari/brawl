﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    public partial class Launcher : Form
    {
        public Launcher()
        {
            InitializeComponent();
            //Random random = new Random();
            //playerTxt.Text = "" + (char)(random.Next(65, 91)) + (char)(random.Next(65, 91)) + (char)(random.Next(65, 91));
            classCombo.DataSource = Enum.GetValues(typeof(Class));
            backgroundPBox.Size = new Size(ClientSize.Width, ClientSize.Height);
            backgroundPBox.Image = new Bitmap(ClientSize.Width, ClientSize.Height);
            backgroundPBox.SendToBack();
            Image title = Image.FromFile("assets\\background\\title.png");
            Graphics graphics = Graphics.FromImage(backgroundPBox.Image);
            graphics.DrawImage(title, backgroundPBox.Size.Width / 2 - title.Width / 2, 15);
            graphics.Dispose();
            infoBtn.Image = Image.FromFile("assets\\background\\info.png");
        }
        IPAddress coso;
        private void goodToStart(object sender, EventArgs e)
        {
            bool good = true;

            if (playerTxt.Text.Trim().Length > 0)
            {
                for (int i = 0; i < playerTxt.Text.Trim().Length && good; i++)
                {
                    if (playerTxt.Text.Trim()[i] >= 128)
                    {
                        good = false;
                    }
                }
            }
            else
            {
                good = false;
            }
            if (good)
            {
                playerTxt.ForeColor = Color.Black;
            }
            else
            {
                playerTxt.ForeColor = Color.Red;
                good = false;
            }

            good = good && (joinRadio.Checked || createRadio.Checked);
            addressTxt.Enabled = joinRadio.Checked;
            addressLbl.Enabled = joinRadio.Checked;

            int port = 0;
            if (portTxt.Text.Trim().Length > 0 && Int32.TryParse(portTxt.Text, out port) && port <= 49151 && port >= 1024)
            {
                portTxt.ForeColor = Color.Black;
            } else {
                portTxt.ForeColor = Color.Red;
                good = false;
            }

            if (joinRadio.Checked)
            {
                bool thisGood = IPAddress.TryParse(addressTxt.Text, out coso);
                try
                {
                    string[] s = addressTxt.Text.Split('.');
                    if (s.Length != 4)
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    good = false;
                }
                good = good && IPAddress.TryParse(addressTxt.Text, out coso);
            }

            startBtn.Enabled = good;
        }
        
        private void startBtn_Click(object sender, EventArgs e)
        {
            Mode mode;
            IPAddress ip;
            int port = Int32.Parse(portTxt.Text);
            if (joinRadio.Checked)
            {
                ip = IPAddress.Parse(addressTxt.Text);
                mode = Mode.Joiner;
            }
            else
            {
                ip = IPAddress.Any;
                mode = Mode.Host;
            }
            Class playerClass;
            Enum.TryParse<Class>(classCombo.SelectedValue.ToString(), out playerClass);
            GameWindow form = new GameWindow(mode, ip, port, playerTxt.Text, playerClass);
            try
            {
                this.Visible = false;
                form.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRORE\n" + ex.ToString());
            }
            this.Visible = true;
        }

        private void infoBtn_Click(object sender, EventArgs e)
        {
            string text =
                "CRUSADER\n" +
                "Il crociato ha una abilità di tipo difensivo: dimezza tutti i danni ricevuti. E' molto utile per difendersi dalle abilità delle altre classi, quando lo scudo non è più disponibile.\n\n" +
                "VIKING\n" +
                "Il vichingo raddoppia per un brevissimo tempo i danni inflitti. Risulta efficace quando l'avversario non può più evocare lo scudo.\n\n" +
                "SARACEN\n" +
                "Il saraceno invoca una tempesta di sabbia, che infligge lievi danni nelle prossimità del giocatore. Bloccare l'avversario ai lati dell'arena è un'ottima strategia.\n\n" +
                "SAMURAI\n" +
                "Il samurai può scattare, a terra o in volo, verso la direzione selezionata, lasciando una scia di colpi mortale lungo il tragitto. Può anche essere usata come fuga rapida.";
            MessageBox.Show(text, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
